using Calendar.App.Authentication;
using Calendar.App.Services;
using Calendar.Shared.Model.Interfaces;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Calendar.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<Calendar.App.App>("app");
            
            builder.Services.AddOptions();                             
            builder.Services.AddAuthorizationCore();                        
            builder.Services.AddScoped<ISessionStorageService, SessionStorageService>();
            builder.Services.AddScoped<IEventDataService, EventDataService>();
            builder.Services.AddScoped<IUserDataService, UserDataService>();
            builder.Services.AddHttpClient<IAuthenticationService, AuthenticationService>(client => client.BaseAddress = new Uri(builder.Configuration["Server"]));
            builder.Services.AddHttpClient<IHttpService, HttpService>(client => client.BaseAddress = new Uri(builder.Configuration["Server"]));
                                 
            builder.Services.AddScoped<UserAuthenticationStateProvider>();
            builder.Services.AddScoped<AuthenticationStateProvider>(p => p.GetService<UserAuthenticationStateProvider>());                      

            await builder.Build().RunAsync();
        }
    }
}
