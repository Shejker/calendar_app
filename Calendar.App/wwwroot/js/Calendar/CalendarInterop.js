﻿let calendar = undefined;
let dotNetReference = undefined;

function SetDotNetReference(reference) {
    dotNetReference = reference;
};

function Render(element, events) {      
    calendar = new FullCalendar.Calendar(element, {
        headerToolbar: {
            left: 'prev,next',
            center: 'title',
            right: 'today'
        },            
        height: '100%',
        eventDisplay: "block",
        eventClick: function (info) { 
            console.log(info.el.style);
            dotNetReference.invokeMethodAsync('EditEvent', info.event.id);
        },       
        eventMouseEnter: function (info) {
            info.el.style.cursor = "pointer";            
            info.el.style.borderColor = "#FFFFFFFF";
        },
        eventMouseLeave: function (info) {           
            info.el.style.borderColor = "#00FFFFFF";
        },
        initialView: 'dayGridWeek',
        locale: "pl",
        loading: function (isLoading) {
            dotNetReference.invokeMethodAsync('ToggleFetch', isLoading);
        },
        //initialDate: '2017-02-15',
        //navLinks: true, // can click day/week names to navigate views
        //editable: true,
        dayMaxEvents: true, // allow "more" link when too many events             
        events: function (info, succ, err) {
            dotNetReference.invokeMethodAsync('FetchEvents', info.end).then(data => {
                ClearEvents();
                var elements = JSON.parse(data);                               
                succ(elements);
            });
        },
        eventDidMount: function (info) {
            
            if (info.event.extendedProps.priority == 1) {
                if (info.event.extendedProps.status) {
                    info.el.style.opacity = 0.5;
                    info.el.style.backgroundColor = "#8bc34a";
                    info.el.style.borderColor = "#8bc34a";
                }
                else {
                    info.el.style.opacity = 1;
                    info.el.style.backgroundColor = "#8bc34a";
                    info.el.style.borderColor = "#8bc34a";
                }
            }
            else if (info.event.extendedProps.priority == 2) {
                if (info.event.extendedProps.status) {
                    info.el.style.opacity = 0.5;
                    info.el.style.backgroundColor = "#DB4218";
                    info.el.style.borderColor = "#DB4218";
                }
                else {
                    info.el.style.opacity = 1;
                    info.el.style.backgroundColor = "#DB4218";
                    info.el.style.borderColor = "#DB4218";
                }
            }   
            else if (info.event.extendedProps.priority == 0) {
                if (info.event.extendedProps.status) {
                    info.el.style.opacity = 0.5;                    
                }
                else {
                    info.el.style.opacity = 1;                    
                }
            }    
        }
    });    
    calendar.render();
}

function RefetchEvents() {
    calendar.refetchEvents();
}

function ClearEvents() {
    var events = calendar.getEvents();
    events.forEach(ev => {
        ev.remove();
    });
}

function UpdateEvents(json)
{ 
    //var sources = calendar.getEventSources();
    ClearEvents();
    var newEvents = JSON.parse(json);
    
    newEvents.forEach(ev => {
        calendar.addEvent(ev);
    });
    //sources.forEach(source => {
    //    source.remove();
    //});        
      
    //calendar.addEventSource(JSON.parse(json));   
}
