﻿self.addEventListener('fetch', event => {
    // You can add custom logic here for controlling whether to use cached data if offline, etc.
    // The following line opts out, so requests go directly to the network as usual.
    return null;
});

self.addEventListener('push', event => {
    try
    {
        var payload = event.data.json();
    }
    catch
    {        
        payload = Object.create(null);
        payload.message = event.data.text();
        payload.url = event.url;
    }
    event.waitUntil(
        self.registration.showNotification('CalendarApp', {
            body: payload.message,
            icon: '~/favicon.ico',
            vibrate: [100, 50, 100],
            data: { url: payload.url }
        })
    );
});

self.addEventListener('notificationclick', event => {
    event.notification.close();
    event.waitUntil(clients.openWindow(event.notification.data.url));
});