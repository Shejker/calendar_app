﻿using Calendar.Shared.Model.Interfaces;
using Microsoft.JSInterop;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace Calendar.App.Services
{
    public class SessionStorageService : ISessionStorageService
    {
        private IJSRuntime _jsRuntime;

        public SessionStorageService(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public async Task<T> GetItem<T>(string key)
        {
            try
            {
                var json = await _jsRuntime.InvokeAsync<string>("sessionStorage.getItem", key);

                if (json == null)
                    return default;

                return JsonSerializer.Deserialize<T>(json);
            }
            catch(Exception ex)
            {
                Console.WriteLine("[ERROR]SessionStorageService: {0} {1}", ex.GetType(), ex.Message);
                return default(T);
            }
            
        }

        public async Task SetItem<T>(string key, T value)
        {
            await _jsRuntime.InvokeVoidAsync("sessionStorage.setItem", key, JsonSerializer.Serialize(value));
        }

        public async Task RemoveItem(string key)
        {
            await _jsRuntime.InvokeVoidAsync("sessionStorage.removeItem", key);
        }
    }
}
