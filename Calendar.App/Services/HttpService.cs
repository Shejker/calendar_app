﻿using Calendar.Shared.Model.Interfaces;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Calendar.App.Services
{
    public class HttpService : IHttpService
    {
        private readonly HttpClient _httpClient;
        private IAuthenticationService _authenticationService;

        public HttpService(HttpClient httpClient, IAuthenticationService authenticationService)
        {           
            _authenticationService = authenticationService;
            _httpClient = httpClient;          
        }

        public async Task<HttpResponseMessage> DeleteAsync(string uri)
        {            
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", await _authenticationService.GetTokenAsync());
            return await _httpClient.DeleteAsync(uri);
        }

        public async Task<Stream> GetStreamAsync(string uri)
        {            
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", await _authenticationService.GetTokenAsync());
            return await _httpClient.GetStreamAsync(uri);                     
        }

        public async Task<string> GetStringAsync(string uri)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", await _authenticationService.GetTokenAsync());
            return await _httpClient.GetStringAsync(uri);
        }

        public async Task<HttpResponseMessage> PatchAsync(string uri, HttpContent content)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", await _authenticationService.GetTokenAsync());
            return await _httpClient.PatchAsync(uri, content);
        }

        public async Task<HttpResponseMessage> PostAsync(string uri, HttpContent content)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", await _authenticationService.GetTokenAsync());
            return await _httpClient.PostAsync(uri, content);
        }

        public async Task<HttpResponseMessage> PutAsync(string uri, HttpContent content)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", await _authenticationService.GetTokenAsync());
            return await _httpClient.PutAsync(uri, content);
        }
    }
}
