﻿using Calendar.App.Json;
using Calendar.Shared.Model;
using Calendar.Shared.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Calendar.App.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private HttpClient _httpClient;       
        private ISessionStorageService _sessionStorageService;     

        public event EventHandler<bool> LoginChanged;

        public User User;

        public AuthenticationService(HttpClient httpClient, ISessionStorageService sessionStorageService)
        {           
            _httpClient = httpClient;           
            _sessionStorageService = sessionStorageService;           
        }   
        
        private async Task<string> GetUsername()
        {
            try
            {
                var encoded = await _sessionStorageService.GetItem<string>("token");
                if (encoded == null)
                    return null;
                var decodedToken = Encoding.ASCII.GetString(Convert.FromBase64String(encoded));
                return decodedToken.Substring(0, decodedToken.IndexOf(':'));
            }
            catch
            {
                return null;
            }
        }

        private async Task<string> GetPassword()
        {
            try
            {
                var encoded = await _sessionStorageService.GetItem<string>("token");
                if (encoded == null)
                    return null;
                var decodedToken = Encoding.ASCII.GetString(Convert.FromBase64String(encoded));
                return decodedToken.Substring(decodedToken.IndexOf(':')+1);
            }
            catch
            {
                return null;
            }
        }

        public async Task<bool> Authenticate(string username = null, string password = null)
        {
            if (username == null)
            {
                username = await GetUsername();
                password = await GetPassword();
                if (username == null)
                    return false;
            }

            var serializerOptions = new JsonSerializerOptions();
            serializerOptions.Converters.Add(new ApiUserConverter());

            var authToken = Encoding.ASCII.GetBytes($"{username}:{password}");
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(authToken));
            try
            {
                User = await JsonSerializer.DeserializeAsync<User>(await _httpClient.GetStreamAsync("/users"), serializerOptions);
                if (User != null)                                
                    return true;
                
            }
            catch
            {
                //
            }
            return false;
        }

        public async Task Login(string username, string password)
        {
            var serializerOptions = new JsonSerializerOptions();
            serializerOptions.Converters.Add(new ApiUserConverter());

            var authToken = Encoding.ASCII.GetBytes($"{username}:{password}");           
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(authToken));
            try
            {
                User = await JsonSerializer.DeserializeAsync<User>(await _httpClient.GetStreamAsync("/users"), serializerOptions);
                if (User != null)
                {
                    await _sessionStorageService.SetItem("token", Convert.ToBase64String(authToken));                    
                    await _sessionStorageService.SetItem("roles", Convert.ToBase64String(Encoding.ASCII.GetBytes(JsonSerializer.Serialize(User.Roles))));
                    LoginChanged?.Invoke(this, true);
                }
            }
            catch(Exception ex)
            {
                //
            }
        }

        public async Task Logout()
        {
            User = null;
            await _sessionStorageService.RemoveItem("token");
            await _sessionStorageService.RemoveItem("roles");
            LoginChanged?.Invoke(this, false);         
        }        

        public async Task<string> GetTokenAsync()
        {            
            try
            {
                return await _sessionStorageService.GetItem<string>("token");
            }
            catch
            {                
                return null;
            }
        }

        public async Task<IEnumerable<string>> GetRolesAsync()
        {
            try
            {                
                var encoded = await _sessionStorageService.GetItem<string>("roles");
                if (encoded == null)
                    return null;
                var json = Encoding.ASCII.GetString(Convert.FromBase64String(encoded));
                return JsonSerializer.Deserialize<List<string>>(json);
            }
            catch
            {                
                return null;
            }
        }

        public User GetUser()
        {            
            return User;            
        }
    }
}
