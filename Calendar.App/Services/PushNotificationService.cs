﻿using Calendar.App.Json;
using Calendar.Shared.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using WebPush;

namespace Calendar.App.Services
{
    public class PushNotificationService
    {        
        //userId, sub
        private Dictionary<string, List<NotificationSubscription>> _subscriptions = new Dictionary<string, List<NotificationSubscription>>();
        private HttpClient _httpClient;
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        private async Task<IEnumerable<Event>> GetEvents(DateTime date)
        {
            try
            {
                JsonSerializerOptions options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;
                options.Converters.Add(new ApiEventsConverter());

                return await JsonSerializer.DeserializeAsync<IEnumerable<Event>>(await _httpClient.GetStreamAsync("/events?date=" + date.ToString("yyyy-MM-dd")), options);
            }
            catch (Exception ex)
            {
                Console.WriteLine("EventDataService: {0} {1}", ex.GetType(), ex.Message);
                return null;
            }
        }

        private async Task EventsMonitor()
        {
            int eventsCheckInterval = 300000; // 5min 300000           
            while (!_cancellationTokenSource.IsCancellationRequested)
            {                
                try
                {
                    var events = await GetEvents(DateTime.Now);
                    if (events == null)
                    {
                        await Task.Delay(5000);
                        continue;
                    }
                    foreach (var @event in @events)
                    {
                        var minutesRemaining = (@event.Date - DateTime.Now).TotalMinutes;
                        if (minutesRemaining <= 60 && minutesRemaining >= 55)
                            _ = SendNotificationAsync(@event, "Do planowanego zadania pozostała niecała godzina!");
                        else if (minutesRemaining<=10 && minutesRemaining>=5)
                            _ = SendNotificationAsync(@event, "Do planowanego zadania pozostało niecałe 10min!");
                    }                    
                }
                catch
                {                    
                    //
                }
                await Task.Delay(eventsCheckInterval); 
            }            
        }

        public PushNotificationService(HttpClient httpClient, string apiKey)
        {
            _httpClient = httpClient;
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", apiKey);
            _ = EventsMonitor();
        }        
                
        public void SubscribeToNotifications(User user, NotificationSubscription subscription)
        {
            if (_subscriptions.TryGetValue(user.Login, out var subs))
            {
                subs.Add(subscription);
            }
            else
            {
                _subscriptions.Add(user.Login, new List<NotificationSubscription>() { subscription });
            }                       
        }

        public async Task SendNotificationAsync(Event @event, string message)
        {            
            var publicKey = "BNTWLKqfP3Znsab0tfHmAjRup93QT6RYf3Xa8_I5Jwe-S8C2-yFSWinKwK9tWwpTygLTB6ujtjI6BJV6SKh6KE0";
            var privateKey = "25bdaUQksQRJMfEzDguE5qvNNBu8SU-autzgKfQ0D30";

            foreach(var user in @event.Users)
            {
                if (!_subscriptions.TryGetValue(user, out var subs))
                    continue;

                foreach(var sub in subs)
                {
                    var pushSubscription = new PushSubscription(sub.Url, sub.P256dh, sub.Auth);
                    var vapidDetails = new VapidDetails("mailto:piotrekhofi@o2.pl", publicKey, privateKey);
                    var webPushClient = new WebPushClient();
                    try
                    {
                        var payload = JsonSerializer.Serialize(new
                        {
                            message,
                            url = $"/",
                        });
                        await webPushClient.SendNotificationAsync(pushSubscription, payload, vapidDetails);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine("Error sending push notification: " + ex.Message);
                    }
                }
            }          
        }
    }
}
