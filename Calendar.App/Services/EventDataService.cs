﻿using Calendar.App.Json;
using Calendar.Shared.Model;
using Calendar.Shared.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Calendar.App.Services
{
    public class EventDataService : IEventDataService
    {       
        private readonly string getEndpointUrl = "/events?date=";
        private readonly string otherEndpointUrl = "/events";        
        private IHttpService _httpService { get; set; }        

        public EventDataService(IHttpService httpService)
        {
            _httpService = httpService;
        }

        public async Task<HttpResponseMessage> AddEvent(Event @event)
        {
            JsonSerializerOptions options = new JsonSerializerOptions();
            options.PropertyNameCaseInsensitive = true;
            options.Converters.Add(new ApiEventConverter());

            var eventJson = JsonSerializer.Serialize(@event, options);
            var content = new StringContent(eventJson, Encoding.UTF8, "application/json");
            return await _httpService.PostAsync(otherEndpointUrl, content);           
        }

        public async Task<HttpResponseMessage> DeleteEvent(int eventId)
        {
            return await _httpService.DeleteAsync(otherEndpointUrl + "/" + eventId.ToString());            
        }        

        public async Task<IEnumerable<Event>> GetEvents(DateTime date)
        {
            try
            {
                JsonSerializerOptions options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;
                options.Converters.Add(new ApiEventsConverter());
                
                return await JsonSerializer.DeserializeAsync<IEnumerable<Event>>(await _httpService.GetStreamAsync(getEndpointUrl + date.ToString("yyyy-MM-dd")), options);
            }
            catch(Exception ex)
            {
                Console.WriteLine("EventDataService: {0} {1}", ex.GetType(), ex.Message);                
                return null;
            }           
        }

        public async Task<HttpResponseMessage> UpdateEvent(Event @event)
        {
            JsonSerializerOptions options = new JsonSerializerOptions();
            options.PropertyNameCaseInsensitive = true;
            options.Converters.Add(new ApiEventConverter());

            var json = JsonSerializer.Serialize(@event, options);
            var eventJson = new StringContent(json, Encoding.UTF8, "application/json");
            return await _httpService.PatchAsync(otherEndpointUrl + $"/{@event.Id}", eventJson);            
        }
    }
}
