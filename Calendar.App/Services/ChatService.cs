﻿using Calendar.Shared.Model;
using Calendar.Shared.Model.Interfaces;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calendar.App.Services
{
    public class ChatService : IChatService, IDisposable
    {        
        private HubConnection _hubConnection;       
        private IAuthenticationService _authenticationService;
        private NavigationManager _navigationManager;

        public event EventHandler MessageReceived;
        public event EventHandler StatusReceived;
        public IList<ChatUserStatus> UsersList { get; private set; } = new List<ChatUserStatus>();
        public IList<ChatMessage> Messages { get; private set; } = new List<ChatMessage>();

        public ChatService(IAuthenticationService authenticationService, NavigationManager navigationManager)
        {
            _authenticationService = authenticationService;
            _navigationManager = navigationManager;
            _ = Initialize();            
        }

        private async Task Initialize()
        {
            _hubConnection = new HubConnectionBuilder()
                .WithUrl(_navigationManager.ToAbsoluteUri("/chathub"))
                .Build();

            _hubConnection.On<ChatMessage>("MessageReceived", (message) =>
            {
                Messages.Add(message);
                MessageReceived?.Invoke(this, EventArgs.Empty);
            });

            _hubConnection.On<ChatUserStatus>("StatusReceived", (status) =>
            {
                var user = UsersList.FirstOrDefault(u => u.Owner == status.Owner);
                user.Status = status.Status;
                StatusReceived?.Invoke(this, EventArgs.Empty);
            });
            _hubConnection.On<List<ChatMessage>>("MessagesReceived", (messages) =>
            {
                Messages = messages;
                MessageReceived?.Invoke(this, EventArgs.Empty);
            });
            
            _hubConnection.On<List<ChatUserStatus>>("StatusListReceived", (list) =>
            {
                UsersList = list;
                StatusReceived?.Invoke(this, EventArgs.Empty);
            });

            await _hubConnection.StartAsync();
            await _hubConnection.SendAsync("Register", await _authenticationService.GetTokenAsync());
        }

        public async Task GetMessages(int count)
        {
            await _hubConnection.SendAsync("GetMessages", count);
        }

        public async Task SendMessage(string message)
        {
            await _hubConnection.SendAsync("SendMessage", new ChatMessage { Owner = await _authenticationService.GetTokenAsync(), Message = message });
        }

        public async Task SetStatus(UserStatus status)
        {
            await _hubConnection.SendAsync("SetStatus", new ChatUserStatus { Owner = await _authenticationService.GetTokenAsync(), Status = status });
        }

        public async void Dispose()
        {
            if (_hubConnection != null)
                await _hubConnection.StopAsync();
        }
    }
}
