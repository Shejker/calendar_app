﻿using Calendar.App.Json;
using Calendar.Shared.Model;
using Calendar.Shared.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Calendar.App.Services
{
    public class UserDataService : IUserDataService
    {
        private readonly string getEndpointUrl = "/users/all";
        private readonly string otherEndpointUrl = "/users";

        private IHttpService _httpService { get; set; }
       
        public UserDataService(IHttpService httpService)
        {
            _httpService = httpService;
        }

        public async Task<HttpResponseMessage> AddUser(User user)
        {
            JsonSerializerOptions options = new JsonSerializerOptions();
            options.PropertyNameCaseInsensitive = true;
            options.Converters.Add(new ApiUserConverter());

            var json = JsonSerializer.Serialize(user, options);            
            var eventJson = new StringContent(json, Encoding.UTF8, "application/json");
            return await _httpService.PostAsync(otherEndpointUrl, eventJson);
        }

        public async Task<HttpResponseMessage> DeleteUser(int userId)
        {
            return await _httpService.DeleteAsync(otherEndpointUrl + "/" + userId.ToString());          
        }

        public async Task<User> GetUser()
        {
            JsonSerializerOptions options = new JsonSerializerOptions();
            options.PropertyNameCaseInsensitive = true;
            options.Converters.Add(new ApiUserConverter());

            return await JsonSerializer.DeserializeAsync<User>(await _httpService.GetStreamAsync(otherEndpointUrl), options);
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            JsonSerializerOptions options = new JsonSerializerOptions();
            options.PropertyNameCaseInsensitive = true;
            options.Converters.Add(new ApiUsersConverter());
            try
            {
                var result = await JsonSerializer.DeserializeAsync<IEnumerable<User>>(await _httpService.GetStreamAsync(getEndpointUrl), options);
                return result;
            }
            catch(Exception ex)
            {
                Console.WriteLine("GetUsersService: {0} {1}", ex.GetType(), ex.Message);
            }
            return null;
            
        }

        public async Task<HttpResponseMessage> UpdateUser(User user)
        {
            JsonSerializerOptions options = new JsonSerializerOptions();
            options.PropertyNameCaseInsensitive = true;
            options.Converters.Add(new ApiUserConverter());

            var json = JsonSerializer.Serialize(user, options);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            return await _httpService.PutAsync(otherEndpointUrl+$"/{user.Id}", content);            
        }

        public async Task<HttpResponseMessage> ResetPassword(User user)
        {
            JsonSerializerOptions options = new JsonSerializerOptions();
            options.PropertyNameCaseInsensitive = true;
            options.Converters.Add(new ApiUserConverter());

            var json = JsonSerializer.Serialize(user, options);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            return await _httpService.PutAsync(otherEndpointUrl + "/0/password", content);
        }
    }
}
