﻿using Calendar.Shared.Model.Interfaces;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Calendar.App.Authentication
{
    public class UserAuthenticationStateProvider : AuthenticationStateProvider
    {
        private IAuthenticationService _authenticationService;        

        public UserAuthenticationStateProvider(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
            _authenticationService.LoginChanged += AuthenticationService_LoginChanged;
        }

        private void AuthenticationService_LoginChanged(object sender, bool e)
        {
            NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());            
        }

        public async Task Login(string username, string password)
        {
            await _authenticationService.Login(username, password);
        }

        public async Task Logout()
        {
            await _authenticationService.Logout();
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {            
            var token = await _authenticationService.GetTokenAsync();
            var roles = await _authenticationService.GetRolesAsync();
            if (token == null || roles == null)
                return new AuthenticationState(new ClaimsPrincipal());

            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Hash, token),                
            }, "User Authentication");

            
            foreach (var role in roles)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, role));                
            }
                
                        
            var user = new ClaimsPrincipal(identity);            
            return new AuthenticationState(user);
        }        
    }
}
