﻿using Calendar.Shared.Model;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Calendar.App.Json
{
    public class ApiUsersConverter : JsonConverter<IEnumerable<User>>
    {
        public override IEnumerable<User> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var users = new List<User>();
            using JsonDocument jsonDocument = JsonDocument.ParseValue(ref reader);

            JsonElement root = jsonDocument.RootElement;
            var count = root.GetArrayLength();
            if (count < 1)
                return users;

            foreach(var jsonUser in root.EnumerateArray())
            {
                var user = new User();
                foreach (var property in jsonUser.EnumerateObject())
                {
                    switch (property.Name)
                    {
                        case "id":
                            user.Id = property.Value.GetInt32();
                            break;

                        case "userName":
                            user.Login = property.Value.GetString();
                            break;

                        case "email":
                            user.Email = property.Value.GetString();
                            break;

                        case "roles":
                            user.Roles = new List<string>();
                            foreach (var role in property.Value.EnumerateArray())
                                user.Roles.Add(role.GetString());
                            break;

                        case "status":
                            user.IsEnabled = property.Value.GetString() == "enabled" ? true : false;
                            break;

                        case "passwordExpiration":
                            user.ExpirationDate = DateTime.ParseExact(property.Value.GetProperty("date").GetString(), "yyyy-MM-dd HH:mm:ss.ffffff", null);
                            break;
                    }
                }
                users.Add(user);
            }
            
            users.Remove(null);
            return users;
        }

        public override void Write(Utf8JsonWriter writer, IEnumerable<User> value, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }
    }
}
