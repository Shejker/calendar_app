﻿using Calendar.Shared.Model;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Calendar.App.Json
{
    public class ApiEventConverter : JsonConverter<Event>
    {
        public override Event Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var ev = new Event();
            using JsonDocument jsonDocument = JsonDocument.ParseValue(ref reader);

            JsonElement root = jsonDocument.RootElement;
            foreach (var property in root.EnumerateObject())
            {
                switch(property.Name)
                {
                    case "id":
                        ev.Id = Int32.Parse(property.Value.ToString());
                        break;

                    case "name":
                        ev.Title = property.Value.ToString();
                        break;

                    case "date":
                        foreach (var prop in property.Value.EnumerateObject())
                        {
                            switch(prop.Name)
                            {
                                case "date":
                                    ev.Date = DateTime.ParseExact(prop.Value.ToString(), "yyyy-MM-dd HH:mm:ss.ffffff", null, System.Globalization.DateTimeStyles.AssumeLocal);
                                    break;
                            }
                        }                        
                        break;

                    case "description":
                        ev.Description = property.Value.ToString();
                        break;

                    case "users":
                        ev.Users = new List<string>();
                        foreach (var user in property.Value.EnumerateArray())
                            ev.Users.Add(user.GetString());
                        break;

                    case "owner":
                        ev.OwnerId = property.Value.GetInt32();
                        break;
                    case "priority":
                        ev.Priority = (EventPriority)property.Value.GetInt32();
                        break;
                                                
                }                
            }
            return ev;
        }

        public override void Write(Utf8JsonWriter writer, Event value, JsonSerializerOptions options)
        {            
            using JsonDocument document = JsonDocument.Parse(JsonSerializer.Serialize(value));

            JsonElement root = document.RootElement;                       

            writer.WriteStartObject();
            writer.WriteNumber(JsonEncodedText.Encode("id"), root.GetProperty(nameof(Event.Id)).GetInt32());
            writer.WriteString("name", JsonEncodedText.Encode(root.GetProperty(nameof(Event.Title)).GetString()));
            writer.WriteString("date", root.GetProperty(nameof(Event.Date)).GetDateTime());
            writer.WriteString("description", JsonEncodedText.Encode(root.GetProperty(nameof(Event.Description)).GetString()));
            writer.WriteStartArray("users");
            foreach (var user in root.GetProperty(nameof(Event.Users)).EnumerateArray())
            {
                writer.WriteStringValue(user.GetString());
            }
            writer.WriteEndArray();
            writer.WriteString("status", JsonEncodedText.Encode(root.GetProperty(nameof(Event.Status)).GetBoolean() ? "enabled": "disabled"));
            writer.WriteNumber(JsonEncodedText.Encode("priority"), root.GetProperty(nameof(Event.Priority)).GetInt32());
            writer.WriteEndObject();   
        }
    }
}
