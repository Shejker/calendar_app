﻿using Calendar.Shared.Model;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Calendar.App.Json
{
    public class CalendarEventsConverter : JsonConverter<IEnumerable<Event>>
    {
        public override IEnumerable<Event> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }

        public override void Write(Utf8JsonWriter writer, IEnumerable<Event> value, JsonSerializerOptions options)
        {            
            using JsonDocument document = JsonDocument.Parse(JsonSerializer.Serialize(value));

            JsonElement root = document.RootElement;
            int count = root.GetArrayLength();
            
            writer.WriteStartArray();

            if (count>0)
            {
                foreach (var ev in root.EnumerateArray())
                {
                    var extendedProps = new Dictionary<string, object>();
                    writer.WriteStartObject();
                    foreach (JsonProperty property in ev.EnumerateObject())
                    {
                        switch (property.Name)
                        {
                            case nameof(Event.Title):
                                writer.WriteString("title", JsonEncodedText.Encode(property.Value.GetString()));
                                break;
                            case nameof(Event.Date):
                                writer.WriteString("start", property.Value.GetDateTime());
                                break;
                            case nameof(Event.Id):
                                writer.WriteNumber(JsonEncodedText.Encode("id"), property.Value.GetInt32());
                                break;
                            case nameof(Event.Status):
                                extendedProps.Add("status", property.Value.GetBoolean());                                
                                //if (property.Value.GetBoolean())
                                //{
                                //    writer.WriteString("backgroundColor", "#8bc34a");
                                //    writer.WriteString("borderColor", "#8bc34a");                                    
                                //}                                    
                                break;
                            case nameof(Event.Priority):
                                extendedProps.Add("priority", property.Value.GetInt32());
                                //writer.WriteNumber(JsonEncodedText.Encode("priority"), property.Value.GetInt32());
                                break;
                        }
                    }
                    writer.WriteStartObject(JsonEncodedText.Encode("extendedProps"));
                    foreach (var prop in extendedProps)
                    {
                        if (prop.Value.GetType() == typeof(bool))
                            writer.WriteBoolean(JsonEncodedText.Encode(prop.Key), (bool)prop.Value);
                        else if (prop.Value.GetType() == typeof(int))
                            writer.WriteNumber(JsonEncodedText.Encode(prop.Key), (int)prop.Value);
                    }
                    writer.WriteEndObject();
                    writer.WriteEndObject();
                }
            }
            
            writer.WriteEndArray();            
        }
    }
}
