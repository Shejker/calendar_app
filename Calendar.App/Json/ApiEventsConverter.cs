﻿using Calendar.Shared.Model;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Calendar.App.Json
{
    public class ApiEventsConverter : JsonConverter<IEnumerable<Event>>
    {
        public override IEnumerable<Event> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var events = new List<Event>();

            using (JsonDocument jsonDocument = JsonDocument.ParseValue(ref reader))
            {
                JsonElement root = jsonDocument.RootElement;
                int count = root.GetArrayLength();

                if (count == 0)
                    return events;


                foreach (var ev in root.EnumerateArray())
                {
                    var users = new List<string>();
                    ev.TryGetProperty("id", out JsonElement jsonId);
                    ev.TryGetProperty("name", out JsonElement jsonName);
                    ev.TryGetProperty("date", out JsonElement nestedDate);
                    nestedDate.TryGetProperty("date", out JsonElement jsonDate);
                    ev.TryGetProperty("description", out JsonElement jsonDescription);
                    ev.TryGetProperty("users", out JsonElement jsonUsers);
                    foreach (var user in jsonUsers.EnumerateArray())
                        users.Add(user.GetString());
                    ev.TryGetProperty("status", out JsonElement jsonStatus);
                    ev.TryGetProperty("owner", out JsonElement jsonOwner);
                    ev.TryGetProperty("priority", out JsonElement jsonPriority);
                    events.Add(new Event(jsonId.GetInt32(), jsonName.GetString(), jsonDescription.GetString(), DateTime.ParseExact(jsonDate.GetString(), "yyyy-MM-dd HH:mm:ss.ffffff", null, System.Globalization.DateTimeStyles.AssumeLocal), users, jsonStatus.GetString() == "enabled" ? true : false, jsonOwner.GetInt32(), (EventPriority)jsonPriority.GetInt32()));
                }
            }

            return events;
        }

        public override void Write(Utf8JsonWriter writer, IEnumerable<Event> value, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
            
        }
    }
}
