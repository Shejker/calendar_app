﻿using Calendar.Shared.Model;
using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Calendar.App.Json
{
    public class ApiChatMessageConverter : JsonConverter<ChatMessage>
    {
        public override ChatMessage Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }

        public override void Write(Utf8JsonWriter writer, ChatMessage value, JsonSerializerOptions options)
        {
            using JsonDocument document = JsonDocument.Parse(JsonSerializer.Serialize(value));

            JsonElement root = document.RootElement;

            writer.WriteStartObject();            
            writer.WriteString("userName", JsonEncodedText.Encode(root.GetProperty(nameof(ChatMessage.Owner)).GetString()));
            writer.WriteString("message", root.GetProperty(nameof(ChatMessage.Message)).GetString());            
            writer.WriteEndObject();
        }
    }
}
