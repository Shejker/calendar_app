﻿using Calendar.Shared.Model;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Calendar.App.Json
{
    public class ApiUserConverter : JsonConverter<User>
    {
        public override User Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var user = new User();
            using JsonDocument jsonDocument = JsonDocument.ParseValue(ref reader);

            JsonElement root = jsonDocument.RootElement;
                                  
            foreach(var property in root.EnumerateObject())
            {
                switch(property.Name)
                {
                    case "id":
                        user.Id = property.Value.GetInt32();
                        break;

                    case "userName":
                        user.Login = property.Value.GetString();
                        break;

                    case "email":
                        user.Email = property.Value.GetString();
                        break;

                    case "roles":
                        user.Roles = new List<string>();
                        foreach (var role in property.Value.EnumerateArray())
                            user.Roles.Add(role.GetString());
                        break;

                    case "status":
                        user.IsEnabled = property.Value.GetString() == "enabled" ? true : false;
                        break;

                    case "passwordExpiration":
                        user.ExpirationDate = DateTime.ParseExact(property.Value.GetProperty("date").GetString(), "yyyy-MM-dd HH:mm:ss.ffffff", null);
                        break;                    
                }
            }

            return user;            
        }

        public override void Write(Utf8JsonWriter writer, User value, JsonSerializerOptions options)
        {
            using JsonDocument document = JsonDocument.Parse(JsonSerializer.Serialize(value));

            JsonElement root = document.RootElement;

            writer.WriteStartObject();
            writer.WriteNumber(JsonEncodedText.Encode("id"), root.GetProperty(nameof(User.Id)).GetInt32());
            
            if (value.Login != null)
                writer.WriteString("name", JsonEncodedText.Encode(root.GetProperty(nameof(User.Login)).GetString()));
            if (value.Email != null)
                writer.WriteString("email", JsonEncodedText.Encode(root.GetProperty(nameof(User.Email)).GetString()));
            if (value.Password?.Length > 0)
                writer.WriteString("password", JsonEncodedText.Encode(root.GetProperty(nameof(User.Password)).GetString()));
            
            if (value.Roles != null)
            {
                writer.WriteStartArray("roles");
                foreach (var role in root.GetProperty(nameof(User.Roles)).EnumerateArray())
                {
                    writer.WriteStringValue(role.GetString());
                }
                writer.WriteEndArray();
            }
            
            if (value.IsEnabled != null)
                writer.WriteString("status", JsonEncodedText.Encode(root.GetProperty(nameof(User.IsEnabled)).GetBoolean() ? "enabled" : "disabled"));
            
            writer.WriteEndObject();
        }
    }
}
