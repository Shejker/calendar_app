﻿using Calendar.Shared.Model;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Calendar.App.Json
{
    public class ApiChatMessagesConverter : JsonConverter<List<ChatMessage>>
    {
        public override List<ChatMessage> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var chatMessages = new List<ChatMessage>();
            using JsonDocument jsonDocument = JsonDocument.ParseValue(ref reader);

            JsonElement root = jsonDocument.RootElement;
            var count = root.GetArrayLength();
            if (count < 1)
                return chatMessages;

            foreach (var jsonUser in root.EnumerateArray())
            {
                var chatMessage = new ChatMessage();
                foreach (var property in jsonUser.EnumerateObject())
                {
                    switch (property.Name)
                    {
                        case "userName":
                            chatMessage.Owner = property.Value.GetString();
                            break;

                        case "message":
                            chatMessage.Message = property.Value.GetString();
                            break;                       
                    }
                }
                chatMessages.Add(chatMessage);
            }

            chatMessages.Remove(null);
            chatMessages.Reverse();
            return chatMessages;
        }

        public override void Write(Utf8JsonWriter writer, List<ChatMessage> value, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }
    }
}
