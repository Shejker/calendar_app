﻿using Calendar.App.Json;
using Calendar.Shared.Model;
using Calendar.Shared.Model.Interfaces;
using Microsoft.AspNetCore.Components;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Calendar.App.Components
{
    public partial class UserWizard
    {
        [Inject]
        private IUserDataService _userDataService { get; set; }
        private string _selectedRole;

        private void ResetDialog()
        {
            User = new User();            
            IsSaving = false;
            ShowError = false;
        }
       
        private async Task<string> ParseErrorCode(HttpResponseMessage response)
        {
            try
            {
                var temp = JsonSerializer.Deserialize<string[]>(await response.Content.ReadAsByteArrayAsync());
                return temp[0];
            }
            catch
            {
                switch (response.StatusCode)
                {
                    case HttpStatusCode.BadRequest:
                        return "Błędne zapytanie";

                    case HttpStatusCode.Conflict:
                        return "Użytkownik już istnieje!";                        

                    case HttpStatusCode.Unauthorized:
                        return "Brak uprawnień!";                        

                    default:
                        return $"Nieoczekiwany błąd: {response}";                        
                }
            }
            
        }

        protected async Task HandleValidSubmit()
        {
            ShowError = false;

            bool result = false;
            IsSaving = true;

            if (User.Id < 0)
            {
                var response = await _userDataService.AddUser(User);
                if (response.IsSuccessStatusCode)
                {
                    JsonSerializerOptions options = new JsonSerializerOptions();
                    options.PropertyNameCaseInsensitive = true;
                    options.Converters.Add(new ApiUserConverter());

                    var responseUser = JsonSerializer.Deserialize<User>(await response.Content.ReadAsStringAsync(), options);

                    User.Id = responseUser.Id;
                    result = true;
                }
                else
                {
                    ErrorMessage = await ParseErrorCode(response);                    
                }

            }
            else
            {
                var response = await _userDataService.UpdateUser(User);
                result = response.IsSuccessStatusCode;

                if (!result)
                    ErrorMessage = await ParseErrorCode(response);
            }

            IsSaving = false;

            if (result)
            {
                Completed?.Invoke(this, User);
                Close();
            }
            else
            {
                ShowError = true;
            }
        }
        
        public string ErrorMessage { get; set; }
        public bool ShowError { get; set; }
        public bool IsSaving { get; set; }
        public bool ShowDialog { get; set; }
        public User User { get; set; } = new User();
        public string SelectedRole
        {
            get => _selectedRole;
            set
            {
                _selectedRole = value;
                if (value == null)
                    return;

                User.Roles.Clear();
                User.Roles.Add(value);
            }
        }

        public event EventHandler<User> Completed;
       
        public void Show(User user = null)
        {
            ResetDialog();
            if (user != null)
            {
                User = new User(user.Id, user.Login, user.Email, user.Roles, user.Password, user.IsEnabled, user.ExpirationDate);
                SelectedRole = user.Roles.FirstOrDefault();
            }
            else
            {
                SelectedRole = "ROLE_MEMBER";
            }
                
            ShowDialog = true;
            StateHasChanged();
        }

        public void Close()
        {
            ShowDialog = false;
            StateHasChanged();
        }        
    }
}
