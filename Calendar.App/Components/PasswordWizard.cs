﻿using Calendar.Shared.Model;
using Calendar.Shared.Model.Interfaces;
using Microsoft.AspNetCore.Components;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Calendar.App.Components
{
    public partial class PasswordWizard
    {
        [Inject]
        private IAuthenticationService _authenticationService { get; set; }
        [Inject]
        private IUserDataService _userDataService { get; set; }
        private void ResetState()
        {
            IsSaving = false;
            ErrorMessage = string.Empty;
            ShowError = false;
            User = new User();
        }

        private string ParseStatusCode(HttpStatusCode code)
        {
            switch(code)
            {
                case HttpStatusCode.BadRequest:
                    return "Błąd komunikacji z serwerem";

                default:
                    return $"Nieoczekiwany błąd: {code}";
            }            
        }

        private async Task ReadCurrentUser()
        {
            await _authenticationService.Authenticate();
            User = _authenticationService.GetUser();
            StateHasChanged();
        }

        private bool RequestConfirmation { get; set; }
        public User User { get; set; } = new User();
        public bool IsSaving { get; set; }
        public string ErrorMessage { get; set; }
        public bool ShowError { get; set; }
        public bool ShowDialog { get; set; }        
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }

        public void Show(bool requestConfirmation, User user = null)
        {
            ResetState();
            RequestConfirmation = requestConfirmation;

            if (user != null)
            {
                User.Id = user.Id;
                User.Login = user.Login;
                User.Password = user.Password;                             
            }
            else
            {
                _ = ReadCurrentUser();            
            }

            ShowDialog = true;
            StateHasChanged();
        }

        public void Close()
        {
            ShowDialog = false;
            StateHasChanged();
        }

        public async Task HandleValidSubmit()
        {
            ShowError = false;
            IsSaving = true;
            bool error = false;

            StateHasChanged();
            
            
            if (User.Password.Length < 8)
            {
                ErrorMessage = "Hasło musi posiadać przynajmniej 8 znaków!";                
                error = true;               
            }

            if (!error && (Regex.Match(User.Password, @"/[a-z]/", RegexOptions.ECMAScript).Success ||
                Regex.Match(User.Password, @"/[A-Z]/", RegexOptions.ECMAScript).Success ||
                Regex.Match(User.Password, @"/.[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]/", RegexOptions.ECMAScript).Success ||
                Regex.Match(User.Password, @"/\d+/", RegexOptions.ECMAScript).Success))
            {
                ErrorMessage = "Hasło musi zawierać małą literę, dużą literę, cyfrę oraz znak!";                
                error = true;          
            }

            if (!error && NewPassword != User.Password)            
            {
                ErrorMessage = "Hasła nie są takie same!";                
                error = true;            
            }

            if (!error)
            {
                if ((RequestConfirmation && await _authenticationService.Authenticate(User.Login, OldPassword)) || (!RequestConfirmation))
                {
                    var result = await _userDataService.UpdateUser(User);
                    if (result.IsSuccessStatusCode)
                    {
                        if (RequestConfirmation)
                            await _authenticationService.Login(User.Login, User.Password);
                        Close();
                    }
                    else
                    {
                        ErrorMessage = ParseStatusCode(result.StatusCode);
                        error = true;
                    }
                }
                else
                {
                    ErrorMessage = "Błędne hasło";
                    error = true;
                }
            }            

            if (error)            
                ShowError = true;                
            
            IsSaving = false;
            StateHasChanged();
        }
    }
}
