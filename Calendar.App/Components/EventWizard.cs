﻿using Calendar.App.Json;
using Calendar.Shared.Model;
using Calendar.Shared.Model.Interfaces;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Calendar.App.Components
{
    public partial class EventWizard : ComponentBase
    {
        [Inject]
        private IEventDataService _eventDataService { get; set; }
        [Inject]
        private IUserDataService _userDataService { get; set; }
        private string _selectedUser;
        private List<User> _users = new List<User>();
        private int _selectedPriority;
        private bool _hasFetchedUsers;
        private bool HasFetchedUsers 
        { 
            get
            {
                _ = FetchUsers();
                return _hasFetchedUsers;
            }
            set
            {
                _hasFetchedUsers = value;
            }
        }

        private void ResetDialog()
        {
            Event = new Event() { Date = DateTime.Now };
            _hasFetchedUsers = false;
            IsSaving = false;
            ShowError = false;
        }

        private async Task FetchUsers()
        {
            if (_hasFetchedUsers)
                return;

            _users = new List<User>(await _userDataService.GetUsers());
            StateHasChanged();
            _hasFetchedUsers = true;
        }

        private async Task<string> ParseErrorCode(HttpResponseMessage response)
        {
            try
            {
                var temp = await JsonSerializer.DeserializeAsync<string[]>(await response.Content.ReadAsStreamAsync());
                return temp[0];
            }
            catch
            {              
                switch (response.StatusCode)
                {
                    case HttpStatusCode.BadRequest:
                        return "Błędne zapytanie.";                        

                    case HttpStatusCode.Unauthorized:
                        return "Brak uprawnień!";                        

                    default:
                        return $"Nieoczekiwany błąd: {response.StatusCode}";                        
                }
            }            
        }

        protected async Task HandleValidSubmit()
        {
            ShowError = false;
            
            bool result = false;
            IsSaving = true;

            if (Event.Id < 0)
            {
                var response = await _eventDataService.AddEvent(Event);
                if (response.IsSuccessStatusCode)
                {
                    JsonSerializerOptions options = new JsonSerializerOptions();
                    options.PropertyNameCaseInsensitive = true;
                    options.Converters.Add(new ApiEventConverter());

                    var responseEvent = JsonSerializer.Deserialize<Event>(await response.Content.ReadAsStringAsync(), options);

                    Event.Id = responseEvent.Id;
                    result = true;
                }
                else
                    ErrorMessage = await ParseErrorCode(response);
            }                
            else
            {
                var response = await _eventDataService.UpdateEvent(Event);
                result = response.IsSuccessStatusCode;

                if (!result)                
                    ErrorMessage = await ParseErrorCode(response);          
                                   
            }
                                            
            IsSaving = false;

            if (result)
            {
                Completed?.Invoke(this, Event);
                Close();                
            }
            else
            {
                ShowError = true;
            }
        }

        protected void DeleteUser(string user)
        {
            Event.Users.Remove(user);
        }

        protected async Task Delete()
        {
            IsSaving = true;
            StateHasChanged();

            var response = await _eventDataService.DeleteEvent(Event.Id);
            if (response.IsSuccessStatusCode)
            {
                Event.OwnerId = -1;
                Close();
                Completed?.Invoke(this, Event);
            }
            else
            {
                ErrorMessage = await ParseErrorCode(response);
                ShowError = true;                
            }

            IsSaving = false;
            StateHasChanged();
        }

        public DateTime Date
        {
            get => Event.Date.ToLocalTime();
            set => Event.Date = value.ToLocalTime();
        }

        public string ErrorMessage { get; set; }
        public bool ShowError { get; set; }
        public Event Event { get; set; } = new Event() { Date = DateTime.Now };
        public bool ShowDialog { get; set; }        
        public bool IsSaving { get; set; }
        public event EventHandler<Event> Completed;
        public IEnumerable<User> RemainingUsers => _users.Where(u => !Event.Users.Contains(u.Login));
        public IEnumerable<User> AllUsers => _users;
        public int SelectedPriority
        {
            get => _selectedPriority;
            set
            {
                _selectedPriority = value;
                Event.Priority = (EventPriority)value;
            }
        }

        public void UserSelected()
        {          
            Event.Users.Add(SelectedUser);
            StateHasChanged();
        }

        //Needs to be here. Magical exception is happening when trying to render MatSelect without @bind-Value...
        public string SelectedUser 
        {
            get => _selectedUser;
            set
            {
                _selectedUser = value;
                UserSelected();
            }
        }
        public void Show(Event @event = null)
        {
            ResetDialog();
            if (@event != null)
            {
                Event = new Event(@event.Id, @event.Title, @event.Description, @event.Date, new List<string>(@event.Users), @event.Status, @event.OwnerId, @event.Priority);
                SelectedPriority = (int)@event.Priority;
            }
            else
            {
                SelectedPriority = 0;
            }
                
            
            ShowDialog = true;
            StateHasChanged();
        }

        public void Close()
        {
            ShowDialog = false;
            StateHasChanged();
        }
    }
}
