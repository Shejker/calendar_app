﻿using Calendar.App.Components;
using Calendar.Shared.Model.Interfaces;
using MatBlazor;
using Microsoft.AspNetCore.Components;

namespace Calendar.App.Shared
{
    public partial class MainLayout
    {
        protected PasswordWizard PasswordDialog;   
        [Inject]
        private IAuthenticationService _authenticationService { get; set; }
        protected void ChangePassword()
        {
            PasswordDialog.Show(true, _authenticationService.GetUser());
        }

        public MatTheme Theme = new MatTheme()
        {
            Primary = "#304ffe",
            Secondary = "#304ffe"
        };


    }
}
