﻿using Calendar.Shared.Model;
using Calendar.Shared.Model.Interfaces;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calendar.App.Pages
{
    public partial class Chat
    {
        private int _userStatus;
        [Inject]
        private IChatService _chatService { get; set; }  
        [Inject]
        private IAuthenticationService _authenticationService { get; set; } 
        

        protected override void OnParametersSet()
        {
            UsersList = _chatService.UsersList;
            Messages = _chatService.Messages;
            
            _chatService.MessageReceived += ChatService_MessageReceived;
            _chatService.StatusReceived += ChatService_StatusReceived;            
        }

        protected override async Task OnParametersSetAsync()
        {            
            if (_authenticationService.GetUser() == null)            
                await _authenticationService.Authenticate();            

            Username = _authenticationService.GetUser().Login;
        }

        private void ChatService_StatusReceived(object sender, System.EventArgs e)
        {
            UsersList = _chatService.UsersList;
            InvokeAsync(() => StateHasChanged());            
        }

        private void ChatService_MessageReceived(object sender, System.EventArgs e)
        {
            Messages = _chatService.Messages;
            InvokeAsync(() => StateHasChanged());
            JS.InvokeVoidAsync("ScrollToBottom", null);
        }
        protected string Username { get; private set; }
        protected bool IsSending { get; set; }
        public IList<ChatUserStatus> UsersList;
        
        public int UserStatus 
        { 
            get => _userStatus; 
            set 
            { 
                _userStatus = value; 
                _chatService.SetStatus((UserStatus)value); 
            } 
        }

        public IList<ChatMessage> Messages;
        public string CurrentMessage { get; set; } = string.Empty;

        public async Task SendMessage()
        {
            IsSending = true;
            StateHasChanged();

            if (CurrentMessage.Length < 1)
                return;

            await _chatService.SendMessage(CurrentMessage);
            CurrentMessage = string.Empty;
            
            IsSending = false;
            StateHasChanged();
        }

        public async Task FetchMoreMessages()
        {
            await _chatService.GetMessages(Messages.Count + 10);
        }

        public async void Enter(KeyboardEventArgs e)
        {
            if (e.Code == "Enter" || e.Code == "NumpadEnter")            
                await SendMessage();            
        }
    }
}
