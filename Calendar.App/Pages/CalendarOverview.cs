﻿using Calendar.App.Components;
using Calendar.App.Json;
using Calendar.Shared.Model;
using Calendar.Shared.Model.Interfaces;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Calendar.App.Pages
{
    public partial class CalendarOverview
    {        
        [Inject]
        private IEventDataService _eventDataService { get; set; }
        private EventWizard _eventWizard;
        private ElementReference calendarContainer;
        private HubConnection _hubConnection;
        [Inject]
        private NavigationManager _navigationManager { get; set; }
        protected EventWizard EventDialog 
        {
            get => _eventWizard;
            set
            {
                if (_eventWizard != null)
                    _eventWizard.Completed -= EventDialog_Completed;
                
                _eventWizard = value;
                
                if (value == null)
                    return;

                _eventWizard.Completed += EventDialog_Completed;                
            }
        }  
        
        private void RefreshEvents(bool fetchApi)
        {
            if (fetchApi)
            {
                JS.InvokeVoidAsync("RefetchEvents");
            }
            else
            {
                var options = new JsonSerializerOptions();
                options.Converters.Add(new CalendarEventsConverter());
                var json = JsonSerializer.Serialize(Events, options);
                JS.InvokeVoidAsync("UpdateEvents", new object[1] { json });
            }
        }

        protected override async Task OnInitializedAsync()
        {
            JS.InvokeVoidAsync("SetDotNetReference", new object[1] { DotNetObjectReference.Create(this) });
            _hubConnection = new HubConnectionBuilder().WithUrl(_navigationManager.ToAbsoluteUri("/calendarhub")).Build();

            _hubConnection.On<Event>("EventReceived", (ev) => 
            {
                var originalEvent = Events.FirstOrDefault(e => e.Id == ev.Id);
                if (originalEvent == null)
                {
                    Events = Events.Concat(new[] { ev });                   
                }
                else
                {                    
                    originalEvent.Title = ev.Title;
                    originalEvent.Status = ev.Status;
                    originalEvent.Date = ev.Date;
                    originalEvent.Description = ev.Description;
                    originalEvent.Status = ev.Status;
                    originalEvent.Users = ev.Users;
                    originalEvent.OwnerId = ev.OwnerId;
                    originalEvent.Priority = ev.Priority;
                }
                RefreshEvents(false);
            });

            _hubConnection.On<int>("EventDeleted", (eventId) =>
            {           
                Events = Events.Where(e => e.Id != eventId);
                RefreshEvents(false);
            });

            await _hubConnection.StartAsync();
        }      

        private void EventDialog_Completed(object sender, Event @event)
        {            
            if (@event.OwnerId == -1)
            {
                _hubConnection.SendAsync("DeleteEvent", @event.Id);
            }
            else
            {
                _hubConnection.SendAsync("EventCreateOrEdit", @event);
            }            
        }

        protected override Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)                         
                JS.InvokeAsync<string>("Render", new object[2] { calendarContainer, Events });
           
            return base.OnAfterRenderAsync(firstRender);
        }

        protected void AddEvent()
        {
            EventDialog.Show();
        }

        public IEnumerable<Event> Events { get; set; } = new List<Event>();
        public bool IsFetching { get; set; }        

        [JSInvokable]
        public void EditEvent(string id)
        {
            if (id == null)
                return;
            EventDialog.Show(Events.FirstOrDefault(e => e.Id == Int32.Parse(id)));
        }

        [JSInvokable]
        public async Task<string> FetchEvents(DateTime date)
        {                        
            Events = await _eventDataService.GetEvents(date);
            
            var options = new JsonSerializerOptions();
            options.Converters.Add(new CalendarEventsConverter());            
            var json = JsonSerializer.Serialize(Events, options);
                      
            return json;            
        }     
        
        [JSInvokable]
        public void ToggleFetch(bool isLoading)
        {
            IsFetching = isLoading;
            StateHasChanged();
        }

        public async ValueTask DisposeAsync()
        {
            await _hubConnection.DisposeAsync();
        }
    }
}
