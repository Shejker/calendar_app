﻿using Calendar.App.Authentication;
using Calendar.App.Services;
using Calendar.Shared.Model;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;
using System.Threading.Tasks;

namespace Calendar.App.Pages
{
    public partial class Index
    {        
        [CascadingParameter]
        private Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        private UserAuthenticationStateProvider _authenticationStateProvider { get; set; }
        [Inject]
        private PushNotificationService _pushNotificationService { get; set; }

        private async Task RequestNotificationSubscriptionAsync()
        {
            var subscription = await JSRuntime.InvokeAsync<NotificationSubscription>("blazorPushNotifications.requestSubscription");
            if (subscription != null)
            {
                _pushNotificationService.SubscribeToNotifications(User, subscription);                                
            }
        }
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            var authState = await authenticationStateTask;

            if (authState.User?.Identity?.IsAuthenticated ?? false)
            {
                NavigationManager.NavigateTo("/Calendar");
            }
        }

        [Inject]
        public NavigationManager NavigationManager { get; set; }
        public User User { get; set; } = new User();
        public string Message { get; set; }
        public bool IsAuthenticating { get; set;}        

        public async Task HandleValidSubmit()
        {
            IsAuthenticating = true;
            await Authenticate();
            IsAuthenticating = false;
        }

        public async Task Authenticate()
        {            
            await _authenticationStateProvider.Login(User.Login, User.Password);

            var authState = await authenticationStateTask;            

            if (authState.User?.Identity?.IsAuthenticated ?? false)
            {
                await RequestNotificationSubscriptionAsync();                
                NavigationManager.NavigateTo("/Calendar");
            }
            else
            {
                Message = "Niepoprawny login lub hasło!";               
            }           
        }

        public void Reset()
        {
            NavigationManager.NavigateTo("/reset");
        }

        public void OnCredentialsChange()
        {            
            if (Message != null)
                Message = null;
        }
    }
}
