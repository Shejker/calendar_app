﻿using Calendar.App.Authentication;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System.Threading.Tasks;

namespace Calendar.App.Pages
{
    public partial class Logout
    {
        [CascadingParameter]
        private Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject]
        private UserAuthenticationStateProvider _authenticationStateProvider { get; set; }        

        protected override async Task OnInitializedAsync()
        {            
            var authState = await authenticationStateTask;

            if (authState.User.Identity?.IsAuthenticated ?? false)            
                await _authenticationStateProvider.Logout();            
            else                         
                NavigationManager.NavigateTo("/");            
        }

        [Inject]
        public NavigationManager NavigationManager { get; set; }
    }
}
