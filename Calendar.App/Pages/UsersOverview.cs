﻿using Calendar.App.Components;
using Calendar.Shared.Model;
using Calendar.Shared.Model.Interfaces;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calendar.App.Pages
{
    public partial class UsersOverview
    {
        [Inject]
        private IUserDataService _userDataService { get; set; }
        private IList<User> Users = new List<User>();
        private UserWizard _userWizard;
        
        private HubConnection _hubConnection;
        [Inject]
        private NavigationManager _navigationManager { get; set; }
        protected PasswordWizard PasswordDialog;
        protected UserWizard UserDialog
        {
            get => _userWizard;
            set
            {
                if (_userWizard != null)
                    _userWizard.Completed -= UserDialog_Completed;

                _userWizard = value;

                if (value == null)
                    return;

                _userWizard.Completed += UserDialog_Completed;
            }
        }       

        protected override async Task OnInitializedAsync()
        {
            _hubConnection = new HubConnectionBuilder().WithUrl(_navigationManager.ToAbsoluteUri("/userhub")).Build();

            _hubConnection.On<User>("UserReceived", (usr) =>
            {
                var originalEvent = Users.FirstOrDefault(e => e.Id == usr.Id);

                if (originalEvent == null)
                {
                    Users.Add(usr);                    
                }
                else
                {                    
                    originalEvent.Login = usr.Login;
                    originalEvent.Email = usr.Email;
                    originalEvent.IsEnabled = usr.IsEnabled;
                    originalEvent.Password = usr.Password;
                    originalEvent.Roles= usr.Roles;                    
                }
                StateHasChanged();
            });

            _hubConnection.On<int>("UserDeleted", (id) =>
            {
                var user = Users.FirstOrDefault(u => u.Id == id);
                Users.Remove(user);
                StateHasChanged();
            });

            await _hubConnection.StartAsync();
        }

        private void UserDialog_Completed(object sender, User e)
        {
            _hubConnection.SendAsync("UserCreateOrEdit", e);
        }

        protected override Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
                _ = FetchUsers();

            return base.OnAfterRenderAsync(firstRender);
        }

        protected bool IsFetching { get; set; }

        protected void AddUser()
        {
            UserDialog.Show();
        }

        protected void EditUser(User user)
        {
            UserDialog.Show(user);
        }

        protected void EditPassword(User user)
        {
            PasswordDialog.Show(false, user);
        }

        protected async Task DeleteUser(User user)
        {
            IsFetching = true;
            StateHasChanged();

            var response = await _userDataService.DeleteUser(user.Id);
            if (response.IsSuccessStatusCode)
                await _hubConnection.SendAsync("DeleteUser", user.Id);
            
            IsFetching = false;
            StateHasChanged();
        }

        private async Task FetchUsers()
        {            
            IsFetching = true;
            StateHasChanged();

            Users = new List<User>(await _userDataService.GetUsers());

            IsFetching = false;
            StateHasChanged();            
        }        
    }
}
