﻿using Calendar.Shared.Model;
using Calendar.Shared.Model.Interfaces;
using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;

namespace Calendar.App.Pages
{
    public partial class Reset
    {
        [Inject]
        private IUserDataService _userDataService { get; set; }
        [Inject]
        private NavigationManager _navigationManager { get; set; }
        public User User { get; set; } = new User();
        public bool IsSending { get; set; }
        public bool Finished { get; set; }
        public string Message { get; set; }
        public bool IsDisabled { get; set; }
        public void ReturnToLogin()
        {
            _navigationManager.NavigateTo("/");
        }

        public async Task HandleValidSubmit()
        {
            IsDisabled = true;
            Message = null;
            StateHasChanged();

            var response = await _userDataService.ResetPassword(User);
            if (response.IsSuccessStatusCode)
            {
                Finished = true;
            }
            else
            {
                switch(response.StatusCode)
                {
                    case System.Net.HttpStatusCode.BadRequest:
                        Message = "Błąd komunikacji z serwerem";
                        break;
                    default:
                        Finished = true;
                        break;
                }
            }
            IsDisabled = false;
            StateHasChanged();
        }
    }
}
