﻿using System.Threading.Tasks;

namespace Calendar.Shared.Model.Interfaces
{
    public interface ISessionStorageService
    {
        Task<T> GetItem<T>(string key);
        Task SetItem<T>(string key, T value);
        Task RemoveItem(string key);
    }
}
