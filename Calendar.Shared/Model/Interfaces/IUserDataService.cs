﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Calendar.Shared.Model.Interfaces
{
    public interface IUserDataService
    {
        Task<IEnumerable<User>> GetUsers();
        Task<User> GetUser();
        Task<HttpResponseMessage> AddUser(User user);
        Task<HttpResponseMessage> UpdateUser(User user);
        Task<HttpResponseMessage> DeleteUser(int userId);
        Task<HttpResponseMessage> ResetPassword(User user);
    }
}
