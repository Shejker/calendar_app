﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calendar.Shared.Model.Interfaces
{
    public interface IChatService
    {              
        Task SendMessage(string message);
        Task SetStatus(UserStatus status);
        IList<ChatMessage> Messages { get; }
        IList<ChatUserStatus> UsersList { get; }
        event EventHandler MessageReceived;
        event EventHandler StatusReceived;
        Task GetMessages(int count);
    }
}
