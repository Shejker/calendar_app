﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Calendar.Shared.Model.Interfaces
{
    public interface IHttpService
    {
        Task<Stream> GetStreamAsync(string uri);    
        Task<HttpResponseMessage> PostAsync(string uri, HttpContent content);
        Task<HttpResponseMessage> DeleteAsync(string uri);
        Task<HttpResponseMessage> PutAsync(string uri, HttpContent content);
        Task<HttpResponseMessage> PatchAsync(string uri, HttpContent content);
        Task<string> GetStringAsync(string uri);
    }
}
