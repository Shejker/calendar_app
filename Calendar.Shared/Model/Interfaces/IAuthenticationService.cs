﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calendar.Shared.Model.Interfaces
{
    public interface IAuthenticationService
    {          
        Task Login(string username, string password);
        Task Logout();
        Task<string> GetTokenAsync();        
        User GetUser();
        /// <summary>
        /// Authenticate user based on provided values.
        /// </summary>
        /// <param name="username">Username for validation. If null, value from session storage is taken</param>
        /// <param name="password">Password for validation. If null, value from session storage is taken</param>
        /// <returns></returns>
        Task<bool> Authenticate(string username = null, string password = null);
        Task<IEnumerable<string>> GetRolesAsync();       
        event EventHandler<bool> LoginChanged;
    }
}
