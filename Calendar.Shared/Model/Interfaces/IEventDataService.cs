﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Calendar.Shared.Model.Interfaces
{
    public interface IEventDataService
    {
        Task<IEnumerable<Event>> GetEvents(DateTime date);        
        Task<HttpResponseMessage> AddEvent(Event @event);
        Task<HttpResponseMessage> UpdateEvent(Event @event);
        Task<HttpResponseMessage> DeleteEvent(int eventId);
    }
}
