﻿using System;
using System.Collections.Generic;

namespace Calendar.Shared.Model
{
    public class Event
    {        
        public Event()
        {

        }
        public Event(int id, string title, string description, DateTime date, IList<string> users, bool status, int ownerId, EventPriority priority)
        {
            Id = id;
            Title = title;
            Description = description;
            Date = date;
            Users = users;
            Status = status;
            OwnerId = ownerId;
            Priority = priority;
        }

        public int Id { get; set; } = -1;
        public string Title { get; set; }       
        public string Description { get; set; }       
        public DateTime Date { get; set; }
        public int OwnerId { get; set; }
        public IList<string> Users { get; set; } = new List<string>();   
        public bool Status { get; set; }
        public EventPriority Priority { get; set; }
    }
}
