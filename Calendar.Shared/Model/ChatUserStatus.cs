﻿namespace Calendar.Shared.Model
{
    public class ChatUserStatus
    {
        public string Owner { get; set; }
        public UserStatus Status { get; set; }
    }
}
