﻿using System;
using System.Collections.Generic;

namespace Calendar.Shared.Model
{    
    public class User
    {
        public User()
        {

        }

        public User(int id, string login, string email, List<string> roles, string password, bool? isEnabled, DateTime expirationDate)
        {
            Id = id;
            Login = login;
            Email = email;
            Roles = roles;
            Password = password;
            IsEnabled = isEnabled;
            ExpirationDate = expirationDate;
        }

        public int Id { get; set; } = -1;
        public string Login { get; set; }
        public string Email { get; set; }
        public List<string> Roles { get; set; } = new List<string>();
        public string Password { get; set; }
        public bool? IsEnabled { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}
