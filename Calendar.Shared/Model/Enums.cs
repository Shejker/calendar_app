﻿namespace Calendar.Shared.Model
{
    public enum UserRole
    {
        AdvancedUser,
        Administrator,
        User
    }    
    public enum UserStatus
    {
        Active,        
        Busy,
        Inactive
    }
    public enum EventPriority
    {
        None,
        Normal,
        Important
    }
}
