﻿namespace Calendar.Shared.Model
{
    public class ChatMessage
    {
        public string Owner { get; set; }
        public string Message { get; set; }
    }
}
