using Calendar.App.Authentication;
using Calendar.App.Services;
using Calendar.Server.Hubs;
using Calendar.Shared.Model.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;

namespace Calendar.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();
            services.AddRazorPages();
            services.AddServerSideBlazor();

            services.AddOptions();
            services.AddAuthorizationCore();

            services.AddScoped<IAuthenticationService, AuthenticationService>(client => new AuthenticationService(new System.Net.Http.HttpClient { BaseAddress = new Uri(Configuration["Server"]) }, client.GetRequiredService<ISessionStorageService>()));
            services.AddHttpClient<IHttpService, HttpService>(client => client.BaseAddress = new Uri(Configuration["Server"]));

            services.AddScoped<ISessionStorageService, SessionStorageService>();
            services.AddScoped<IEventDataService, EventDataService>();
            services.AddScoped<IUserDataService, UserDataService>();            
                       
            services.AddScoped<UserAuthenticationStateProvider>();
            services.AddScoped<AuthenticationStateProvider>(p => p.GetService<UserAuthenticationStateProvider>());
            services.AddSingleton<PushNotificationService>(p => 
            {
                using (var scope = p.CreateScope())
                {
                    return new PushNotificationService(new System.Net.Http.HttpClient { BaseAddress = new Uri(Configuration["Server"]) }, Configuration["APIKey"]);
                }                
            });

            services.AddScoped<IChatService, ChatService>();
            services.AddSingleton<Services.ChatService>(x => new Services.ChatService(Configuration));
            
            services.AddResponseCompression(opts =>
            {
                opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                    new[] { "application/octet-stream" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapHub<UserHub>("/userhub");
                endpoints.MapHub<CalendarHub>("/calendarhub");
                endpoints.MapHub<ChatHub>("/chathub");
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
