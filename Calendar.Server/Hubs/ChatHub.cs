﻿using Calendar.Server.Services;
using Calendar.Shared.Model;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace Calendar.Server.Hubs
{
    public class ChatHub : Hub
    {
        private ChatService _chatService;
        
        public ChatHub(ChatService chatService)
        {
            _chatService = chatService;
            _chatService.StatusChanged += ChatService_StatusChanged;
        }

        protected override void Dispose(bool disposing)
        {            
            _chatService.StatusChanged -= ChatService_StatusChanged;
            base.Dispose(disposing);
        }

        private async void ChatService_StatusChanged(object sender, ChatUserStatus e)
        {
            _chatService.SetStatus(e);
            await Clients.Clients(_chatService.ChatGroup).SendAsync("StatusReceived", e);
        }        

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            _chatService.DisconnectUser(Context.ConnectionId);
            await base.OnDisconnectedAsync(exception);
        }

        public async Task Register(string token)
        {
            _chatService.RegisterConnection(Context.ConnectionId, token);
            await Clients.Caller.SendAsync("StatusListReceived", _chatService.UserList);
            await Clients.Caller.SendAsync("MessagesReceived", await _chatService.GetMessages(10));
            await Clients.Clients(_chatService.ChatGroup).SendAsync("StatusReceived", _chatService.GetStatus(Context.ConnectionId));
        }

        public async Task SetStatus(ChatUserStatus status)
        {
            _chatService.SetStatus(status);
            status.Owner = _chatService.GetUsername(status.Owner);
            await Clients.Clients(_chatService.ChatGroup).SendAsync("StatusReceived", status);
        }

        public async Task SendMessage(ChatMessage chatMessage)
        {
            chatMessage.Owner = _chatService.GetUsername(chatMessage.Owner);
            _chatService.QueueMessage(chatMessage);
            await Clients.Clients(_chatService.ChatGroup).SendAsync("MessageReceived", chatMessage);
        }

        public async Task GetMessages(int count)
        {
            await Clients.Caller.SendAsync("MessagesReceived", await _chatService.GetMessages(count));            
        }                
    }
}
