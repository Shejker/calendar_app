﻿using Calendar.Shared.Model;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Calendar.Server.Hubs
{
    public class UserHub : Hub
    {
        public async Task UserCreateOrEdit(User ev)
        {
            await Clients.All.SendAsync("UserReceived", ev);
        }

        public async Task DeleteUser(int id)
        {
            await Clients.All.SendAsync("UserDeleted", id);
        }
    }
}
