﻿using Calendar.Shared.Model;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Calendar.Server.Hubs
{
    public class CalendarHub : Hub
    {
        public async Task EventCreateOrEdit(Event ev)
        {
            await Clients.All.SendAsync("EventReceived", ev);
        }

        public async Task DeleteEvent(int id)
        {
            await Clients.All.SendAsync("EventDeleted", id);
        }        
    }
}
