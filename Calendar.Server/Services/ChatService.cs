﻿using Calendar.App.Json;
using Calendar.Shared.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Calendar.Server.Services
{
    public class ChatService : IDisposable
    {
        private readonly Dictionary<string, ChatUserStatus> ConnectionList = new Dictionary<string, ChatUserStatus>();
        private readonly Queue<ChatMessage> MessageQueue = new Queue<ChatMessage>();
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1);
        private HttpClient _httpClient = new HttpClient();
        private object _syncObject = new object();

        public ChatService(IConfiguration configuration)
        {
            _httpClient.BaseAddress = new Uri(configuration["Server"]);
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", configuration["APIKey"]);           
            FetchUsers().Wait();
            _ = MessagesQueueHandler();
        }

        private async Task MessagesQueueHandler()
        {
            var options = new JsonSerializerOptions();
            options.Converters.Add(new ApiChatMessageConverter());
            while(!_cancellationTokenSource.IsCancellationRequested)
            {
                if (MessageQueue.TryDequeue(out var message))
                {
                    var content = new StringContent(JsonSerializer.Serialize(message, options));
                    await _httpClient.PostAsync("/chat", content);
                }
                else
                    await _semaphoreSlim.WaitAsync(_cancellationTokenSource.Token);
            }
        }

        private async Task FetchUsers()
        {
            UserList.Clear();
            JsonSerializerOptions options = new JsonSerializerOptions();
            options.PropertyNameCaseInsensitive = true;
            options.Converters.Add(new ApiUsersConverter());

            var users = await JsonSerializer.DeserializeAsync<IEnumerable<User>>(await _httpClient.GetStreamAsync("/users/all"), options);

            foreach (var user in users)
                UserList.Add(new ChatUserStatus { Owner = user.Login, Status = UserStatus.Inactive });
        }
        
        public IReadOnlyList<string> ChatGroup => ConnectionList.Keys.ToList();
        public readonly List<ChatUserStatus> UserList = new List<ChatUserStatus>();
        public event EventHandler<ChatUserStatus> StatusChanged;

        public string GetUsername(string token)
        {
            try
            {
                var auth = Encoding.ASCII.GetString(Convert.FromBase64String(token));                
                return auth.Substring(0, auth.IndexOf(':'));                                   
            }
            catch(Exception ex)
            {                
                if (ex is FormatException || ex is ArgumentOutOfRangeException)
                    return token;

                Console.WriteLine("GetUsername({0}) failed: {1}", token, ex.Message);
                return null;
            }            
        }

        public async Task<List<ChatMessage>> GetMessages(int count)
        {            
            var options = new JsonSerializerOptions();
            options.Converters.Add(new ApiChatMessagesConverter());

            return await JsonSerializer.DeserializeAsync<List<ChatMessage>>(await _httpClient.GetStreamAsync($"/chat?count={count}"), options);
        }

        public ChatUserStatus GetStatus(string connectionId)
        {
            lock (_syncObject)
            {
                return ConnectionList.GetValueOrDefault(connectionId);
            }
        }

        public void RegisterConnection(string connectionId, string token)
        {
            lock(_syncObject)
            {
                if (ConnectionList.ContainsKey(connectionId))
                    return;

                var user = UserList.FirstOrDefault(u => u.Owner == GetUsername(token));

                if (user == null)
                    return;

                user.Status = UserStatus.Active;
                ConnectionList.Add(connectionId, user);
            }            
        }

        public void SetStatus(ChatUserStatus status)
        {
            lock(_syncObject)
            {
                var user = UserList.FirstOrDefault(u => u.Owner == GetUsername(status.Owner));

                if (user == null)
                    return;

                user.Status = status.Status;
            }            
        }

        public void QueueMessage(ChatMessage message)
        {
            MessageQueue.Enqueue(message);
            if (_semaphoreSlim.CurrentCount == 0)
                _semaphoreSlim.Release();
        }

        public void DisconnectUser(string connectionId)
        {
            lock (_syncObject)
            {                
                ConnectionList.TryGetValue(connectionId, out var user);
                ConnectionList.Remove(connectionId);

                if (!ConnectionList.Values.ToList().Contains(user))
                {
                    user.Status = UserStatus.Inactive;
                    StatusChanged?.Invoke(this, user);
                }
            }                                         
        }

        public void Dispose()
        {
            _cancellationTokenSource.Cancel();
        }
    }
}
